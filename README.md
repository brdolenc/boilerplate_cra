## Para iniciar o projeto execute os seguintes comandos

```bash
npm install
#Instalar dependências.

npm start
#Executa o aplicativo no modo de desenvolvimento.
```
## Wiki do projeto
[https://bitbucket.org/brdolenc/boilerplate_cra/wiki/Home](https://bitbucket.org/brdolenc/boilerplate_cra/wiki/Home)

## Para executar os testes

```bash
npm run test
npm run testewatch
npm run coverage
```

## Para gerar build de produção

```bash
npm run build
```

## Para iniciar a CLI do projeto

```bash
cd cli

npm install
#Instalar dependências da CLI.

npm link
```

## Comandos da CLI

> :warning: Sempre execute os comanandos da CLI dentro do root do projeto

```bash

dasa --help
#Lista todos os comandos.

dasa components
#Criar componentes.
```

## Iniciar o Storybook
Listagem de todos os componentes presentes no projeto

```bash
npm run storybook
```
