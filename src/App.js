import React from 'react';
import Button from './components/atoms/button/Button';

function App() {
  return (
    <>
      <h1>{process.env.REACT_APP_NAME}</h1>
      <Button kind="ghost">Botão</Button>
    </>
  );
}

export default App;
