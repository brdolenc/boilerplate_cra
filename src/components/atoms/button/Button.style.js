import styled, { css } from 'styled-components';

export const Button = styled.button`
  font-size: 1rem;
  font-family: inherit;
  font-weight: 400;
  padding: 0.4em 1.2em;
  margin: 0.3rem;
  border-radius: 2rem;
  cursor: pointer;
  outline: 1px solid transparent;
  border: 4px solid transparent;
  transition: all 0.2s ease-in-out;

  /* Type */
  ${({ kind }) =>
    kind === 'default' &&
    css`
      color: #666666;
      background: #f4f4f4;
    `}

  ${({ kind }) =>
    kind === 'solid' &&
    css`
      color: #ffffff;
      background: #000000;

      &:hover {
        background: #f4f4f4;
      }
    `}

  ${({ kind }) =>
    kind === 'ghost' &&
    css`
      color: #000000;
      border: 2px solid #000000;
      background: transparent;

      &:hover {
        color: #000000;
        border: 2px solid transparent;
        background: #f4f4f4;
      }
    `}

  ${({ kind }) =>
    kind === 'link' &&
    css`
      border-radius: 0;
      border: 2px solid transparent;
      color: #000000;
      background: transparent;

      &:hover,
      &:hover a {
        color: #000000;
        text-decoration: underline;
      }
    `}

  ${({ kind }) =>
    kind === 'cta' &&
    css`
      font-size: 0.8rem;
      font-weight: 600;
      padding: 0.4em 2em;
      text-transform: uppercase;
      box-shadow: 0px 10px 5px -10px silver;
      color: #ffffff;
      background: #000000;

      &:hover {
        color: #ffffff;
        background: #f4f4f4;
      }
    `}

    /* Size */
  ${({ size }) =>
    size === 'xsmall' &&
    css`
      font-size: 0.6rem;
    `}

  ${({ size }) =>
    size === 'small' &&
    css`
      font-size: 0.8rem;
    `}

  ${({ size }) =>
    size === 'medium' &&
    css`
      font-size: 1rem;
    `}

  ${({ size }) =>
    size === 'large' &&
    css`
      font-size: 1.2rem;
    `}

  ${({ size }) =>
    size === 'xlarge' &&
    css`
      font-size: 1.4rem;
    `}
`;

Button.defaultProps = {
  size: 'medium',
  kind: 'default',
};
