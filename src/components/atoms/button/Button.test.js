import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Button from './Button';

describe('Testing Button Component', () => {
  it('should render correctly', () => {
    render(<Button>Test</Button>);

    const bt = screen.getByTestId('button');

    expect(bt).toBeInTheDocument();
  });

  it('should trigger on click function', () => {
    const funTest = jest.fn();
    const { getByTestId } = render(<Button onClick={funTest}>Teste</Button>);

    getByTestId('button').click();

    expect(funTest).toHaveBeenCalledTimes(1);
  });

  it('should not trigger event click', () => {
    const funTest = jest.fn();
    const { getByTestId } = render(<Button onClick={funTest}>Teste</Button>);

    getByTestId('button');
    expect(funTest).toHaveBeenCalledTimes(0);
  });

  it('should not trigger on click is button is disabled', () => {
    const funTest = jest.fn();
    const { getByTestId } = render(
      <Button disabled onClick={funTest}>
        Teste
      </Button>,
    );

    getByTestId('button').click();

    expect(funTest).toHaveBeenCalledTimes(0);
  });
});
