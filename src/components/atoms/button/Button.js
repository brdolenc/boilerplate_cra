import React from 'react';
import PropTypes from 'prop-types';

import * as S from './Button.style';

const Button = ({ children, kind, size, onClick, ...attributes }) => {
  return (
    <S.Button
      data-testid="button"
      type="button"
      kind={kind}
      size={size}
      onClick={onClick}
      {...attributes}
    >
      {children}
    </S.Button>
  );
};

Button.propTypes = {
  kind: PropTypes.oneOf(['default', 'solid', 'ghost', 'link', 'cta']),
  size: PropTypes.oneOf(['xsmall', 'small', 'medium', 'large', 'xlarge']),
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  kind: 'default',
  size: 'medium',
  onClick: () => {},
};

export default Button;
