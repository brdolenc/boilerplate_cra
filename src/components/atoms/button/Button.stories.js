import React from 'react';
import { action } from '@storybook/addon-actions';

import Button from './Button';

export default {
  title: 'src/components/atoms/Button',
  component: Button,
};

const Template = args => <Button {...args} onClick={action('cliquei')} />;

export const Default = Template.bind({});
Default.args = {
  kind: 'default',
  children: 'Button',
};

export const Solid = Template.bind({});
Solid.args = {
  kind: 'solid',
  children: 'Button',
};

export const Ghost = Template.bind({});
Ghost.args = {
  kind: 'ghost',
  children: 'Button',
};

export const Link = Template.bind({});
Link.args = {
  kind: 'link',
  children: 'Button',
};

export const Cta = Template.bind({});
Cta.args = {
  kind: 'cta',
  children: 'Button',
};

export const Xsmall = Template.bind({});
Xsmall.args = {
  size: 'xsmall',
  children: 'Button',
};

export const Small = Template.bind({});
Small.args = {
  size: 'small',
  children: 'Button',
};

export const Medium = Template.bind({});
Medium.args = {
  size: 'medium',
  children: 'Button',
};

export const Large = Template.bind({});
Large.args = {
  size: 'large',
  children: 'Button',
};

export const Xlarge = Template.bind({});
Xlarge.args = {
  size: 'xlarge',
  children: 'Button',
};
