module.exports = {
  name: 'component',
  description: 'Create new component',
  run: async (toolbox) => {
    const {
      parameters,
      prompt,
      template,
      strings,
      filesystem,
      system,
      print: { success, error },
    } = toolbox;

    const featuresPath = filesystem.path(__dirname, '..', '..', '..', 'src/features');

    const questions = [
      {
        type: 'input',
        name: 'nameCp',
        message: 'Qual o nome do componente?',
        validate: (value) =>
          (!value && '🚨 Esse campo é obrigatório 🚨') || true,
      },
      {
        type: 'select',
        name: 'contextCp',
        message: 'Qual o contexto do componente?',
        choices: ['Geral', 'Feature'],
      },
      {
        type: (current, prev) => prev.contextCp == 'Feature' ? 'select' : null,
        name: 'feature',
        message: 'Qual feature?',
        choices: filesystem.list(featuresPath)
      },
      {
        type: 'select',
        name: 'typeCp',
        message: prev => `${prev.answers.contextCp} Qual o tipo de componente?`,
        choices: ['Atoms', 'Molecules', 'Organisms', 'Templates', 'Pages']
      }
    ];

    const { nameCp, contextCp, feature, typeCp } = await prompt.ask(questions);

    const project = filesystem.path(__dirname, '..', '..', '..');

    const namePascalCase = strings.pascalCase(nameCp);

    const targetPath = contextCp === 'Geral'
      ? `src/components/${strings.lowerCase(typeCp)}/${namePascalCase}`
      : `src/features/${feature}/${strings.lowerCase(typeCp)}/${namePascalCase}`;

    await template.generate({
      template: 'components/component.js.ejs',
      target: `${project}/${targetPath}/${namePascalCase}.js`,
      props: { namePascalCase },
    });

    await template.generate({
      template: 'components/stories.js.ejs',
      target: `${project}/${targetPath}/${namePascalCase}.stories.js`,
      props: { namePascalCase, targetPath },
    });

    await template.generate({
      template: 'components/style.js.ejs',
      target: `${project}/${targetPath}/${namePascalCase}.style.js`,
      props: { namePascalCase },
    });

    await template.generate({
      template: 'components/test.js.ejs',
      target: `${project}/${targetPath}/${namePascalCase}.test.js`,
      props: { namePascalCase },
    });

    success(`🚀 Componente ${namePascalCase} criado!`);
  },
};
